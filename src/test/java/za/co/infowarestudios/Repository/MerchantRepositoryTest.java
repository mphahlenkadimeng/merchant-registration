package za.co.infowarestudios.Repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import za.co.infowarestudios.Entity.Merchant;
import za.co.infowarestudios.Service.MerchantService;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
/**
 * Created by johanneskekae on 2015/12/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WebAppConfiguration.class)
@WebAppConfiguration
public class MerchantRepositoryTest {

    @Autowired
    MerchantRepository repository;

    @Autowired
    MerchantService service;


}
