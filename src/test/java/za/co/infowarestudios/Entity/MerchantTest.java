package za.co.infowarestudios.Entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by johanneskekae on 2015/12/10.
 */

@RunWith(MockitoJUnitRunner.class)
public class MerchantTest {

    @Mock
    Merchant merchant;

    @Test
    public  void testMerchant(){
        when(merchant.getId()).thenReturn(1L);
        when(merchant.getCellNo()).thenReturn("074123455");
        when(merchant.getBank_account_details()).thenReturn("FNB Account");
        when(merchant.getName()).thenReturn("Joe Man");
        when(merchant.getId_Number()).thenReturn("85012394056950");
        when(merchant.getPin()).thenReturn("1234");
        when(merchant.getResidential_Address()).thenReturn("Midrand");
        when(merchant.getSurname()).thenReturn("Kekae");
        when(merchant.getUserCode()).thenReturn("5432");


        assertEquals(1L,merchant.getId());
        assertEquals("074123455",merchant.getCellNo());
        assertEquals("FNB Account",merchant.getBank_account_details());
        assertEquals("Joe Man",merchant.getName());
        assertEquals("85012394056950",merchant.getId_Number());
        assertEquals("1234",merchant.getPin());
        assertEquals("Midrand",merchant.getResidential_Address());
        assertEquals("Kekae",merchant.getSurname());
        assertEquals("5432",merchant.getUserCode());

    }
}
