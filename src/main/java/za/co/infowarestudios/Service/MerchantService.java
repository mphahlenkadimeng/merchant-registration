package za.co.infowarestudios.Service;
import za.co.infowarestudios.Repository.MerchantRepository;
import  za.co.infowarestudios.Entity.Merchant;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.infowarestudios.Exceptions.MerchantDoesNotExistException;
import java.util.*;

/**
 * Created by johanneskekae on 2015/12/10.
 */
public class MerchantService {
    @Autowired
    MerchantRepository merchantRepository;

    public Merchant createMerchant(final Merchant merchant){

        //creating the merchant
        return  merchantRepository.save(merchant);
    }

    public Merchant updateMerchant(Merchant merchant) {

        //check if the user exist first before updating

        if (merchantRepository.exists(merchant.getId())) {
            return createMerchant(merchant);
        } else
        {
            //return an exception
            throw  new MerchantDoesNotExistException("Merchant does not exist");
        }
    }


    public List<Merchant> getAllMerchants(){
        Iterator iterator = merchantRepository.findAll().iterator();
        List<Merchant> u = new ArrayList<Merchant>();

        while (iterator.hasNext()){
            Merchant merchant = (Merchant) iterator.next();
            u.add(merchant);
        }
        return u;
    }

    public Merchant getMerchantById(final long id) {
        Merchant merchant = merchantRepository.findOne(id);

        // check if the object is empty and throw exception

        if (merchant == null) {

            throw new MerchantDoesNotExistException("Merchant does not exist");
        } else {
            return merchant;
        }
    }



}
