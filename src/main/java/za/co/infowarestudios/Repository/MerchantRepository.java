package za.co.infowarestudios.Repository;

import org.springframework.data.repository.CrudRepository;
import  za.co.infowarestudios.Entity.Merchant;

/**
 * Created by johanneskekae on 2015/12/10.
 */
public interface MerchantRepository extends CrudRepository<Merchant,Long>
{
    // just added for testing
    Merchant findMerchantByCellNo(String cellnum);
}
