package za.co.infowarestudios.Exceptions;

/**
 * Created by johanneskekae on 2015/12/10.
 */
public class MerchantDoesNotExistException extends RuntimeException {
    public MerchantDoesNotExistException(){

    }
    public MerchantDoesNotExistException(String a)
    {
        super(a);
    }

}
