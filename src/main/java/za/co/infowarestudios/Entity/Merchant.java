package za.co.infowarestudios.Entity;

import javax.persistence.*;

/**
 * Created by johanneskekae on 2015/12/10.
 */
@Entity
public class Merchant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String Name;
    private String Surname;
    @Column(name ="Cell_Number")

    private String cellNo;
    @Column(name ="ID_Number" )

    private  String id_Number;
    @Column(name = "Residential_Address")
    private  String residential_Address;

    @Column(name = "Bank_Account_Details")
    private String bank_account_details;

    @Column(name = "UserCode")
    private String userCode;

    @Column(name = "Pin")
    private String pin;

    protected Merchant()
    {

    }

    public Merchant(String Name, String Surname, String cellNo, String Id_Number, String bankDetails, String userCode, String pin)
    {
        this.bank_account_details = bankDetails;
        this.cellNo = cellNo;
        this.id_Number = Id_Number;
        this.Name  = Name;
        this.pin = pin;
        this.userCode = userCode;
        this.Surname = Surname;
    }

    public void setId(long id) {

        this.id = id;
    }

    public long getId(){
        return id;

    }


    public String getName() {

        return Name;
    }


    public void setName(String name) {

        Name = name;
    }


    public String getSurname() {

        return Surname;
    }


    public void setSurname(String surname) {

        Surname = surname;
    }


    public String getCellNo() {

        return cellNo;
    }


    public void setCellNo(String cellNo) {

        this.cellNo = cellNo;
    }


    public String getId_Number() {

        return id_Number;
    }


    public void setId_Number(String id_Number) {

        this.id_Number = id_Number;
    }


    public String getResidential_Address() {

        return residential_Address;
    }


    public void setResidential_Address(String residential_Address) {

        this.residential_Address = residential_Address;
    }


    public String getBank_account_details() {

        return bank_account_details;
    }


    public void setBank_account_details(String bank_account_details) {
        this.bank_account_details = bank_account_details;
    }


    public String getUserCode() {

        return userCode;
    }


    public void setUserCode(String userCode) {

        this.userCode = userCode;
    }


    public String getPin() {

        return pin;
}


public void setPin(String pin) {

        this.pin = pin;
        }

        }
